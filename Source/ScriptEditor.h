/*
  ==============================================================================

    ScriptEditor.h
    Created: 20 Jun 2019 7:06:54pm
    Author:  Mathieu Demange

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ScriptModel.h"

class ScriptEditor : public Component,
                     public ChangeListener
{
public:
    ScriptEditor() : editor (document, &xmlTokeniser)
    {
        scriptModel->addChangeListener (this);
        addAndMakeVisible (editor);
        setSize (800, 600);
    }
    ~ScriptEditor() {}
    
    void changeListenerCallback (ChangeBroadcaster* source) override
    {
        if (source == scriptModel)
        {
            FileInputStream fileInputStream (scriptModel->getSource());
            document.loadFromStream (fileInputStream);
        }
    }
    
    void resized() override
    {
        editor.setBounds (getLocalBounds());
    }
    
private:
    SharedResourcePointer<ScriptModel> scriptModel;
    
    XmlTokeniser xmlTokeniser;
    CodeDocument document;
    CodeEditorComponent editor;
    
    
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ScriptEditor)
};

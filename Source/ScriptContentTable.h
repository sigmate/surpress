/*
  ==============================================================================

    ScriptContentTable.h
    Created: 19 Jun 2019 3:25:09pm
    Author:  Mathieu Demange

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ScriptModel.h"

class ScriptContentTable : public TableListBox,
                           public TableListBoxModel,
                           public ChangeListener
{
public:
    
    enum ColumnIDs
    {
        index = 1,
        text = 2,
        role = 3
    };
    
    ScriptContentTable()
    {
        scriptModel->addChangeListener (this);
        setModel (this);
        getHeader().addColumn ("#", ColumnIDs::index, 80);
        getHeader().addColumn ("Role", ColumnIDs::role, 100);
        getHeader().addColumn ("Text", ColumnIDs::text, 1000);
        setRowHeight (64);
    }
    ~ScriptContentTable() {}
    
    void changeListenerCallback (ChangeBroadcaster* source) override
    {
        if (source == scriptModel)
        {
            updateContent();
            repaint();
            Point<int> pos = getViewport()->getViewPosition();
            pos.y = (scriptModel->getLiveSlide() * getRowHeight() - (getViewport()->getViewHeight() / 2.0));
            getViewport()->setViewPosition (pos);
        }
    }
    
    int getNumRows() override
    {
        return scriptModel->getNumSlides();
    }
    
    void paintRowBackground (Graphics& g, int rowNumber, int width, int height, bool rowIsSelected) override
    {
        auto alternateColour = getLookAndFeel().findColour (ListBox::backgroundColourId)
        .interpolatedWith (getLookAndFeel().findColour (ListBox::textColourId), 0.03f);
        if (rowIsSelected)
            g.fillAll (Colours::lightblue);
        else if (rowNumber == scriptModel->getLiveSlide())
            g.fillAll (Colours::darkorange);
        else if (rowNumber % 2)
            g.fillAll (alternateColour);
    }
    
    void paintCell (Graphics& g, int rowNumber, int columnId,
                    int width, int height, bool rowIsSelected) override
    {
        if (columnId == ColumnIDs::index)
        {
            g.setColour (Colours::white);
            g.drawText (String (rowNumber), 2, 0, width - 4, height, Justification::centredLeft, true);
        }
        else if (columnId == ColumnIDs::text)
        {
            AttributedString text;
            
            for (int i = 0; i < scriptModel->getSlide(rowNumber)->getNumChildElements(); ++i)
            {
                auto part = scriptModel->getSlide(rowNumber)->getChildElement (i);
                String role = part->getStringAttribute (IDs::role, "");
                String style = part->getTagName();
                //String prefix = "";
                String prefix = role == scriptModel->getLastRole() ? "" : scriptModel->getPrefixForStyle (style);
                Font font = scriptModel->getFontForStyle (style);
                font.setHeight (font.getHeight() - 26.f);
                //text.append(prefix + part->getAllSubText(), scriptModel->getFontForStyle (style), scriptModel->getColourForStyle (style));
                text.append(prefix + part->getAllSubText(), font, scriptModel->getColourForStyle (style));
                text.append ("\n");
                //scriptModel->setLastRole (role);
            }
            
            text.setJustification (Justification::centredLeft);
            text.draw (g, Rectangle<float>(0.f, 0.f, width, height));
        }
        else if (columnId == ColumnIDs::role)
        {
            g.setColour (Colours::white);
            StringArray roles;
            for (int i = 0; i < scriptModel->getSlide(rowNumber)->getNumChildElements(); ++i)
            {
                auto part = scriptModel->getSlide(rowNumber)->getChildElement (i);
                roles.add (part->getStringAttribute(IDs::role));
            }
            g.drawText (roles.joinIntoString (", "), 2, 0, width - 4, height, Justification::centredLeft, true);
        }
    }
    
    void sortOrderChanged (int newSortColumnId, bool isForwards) override
    {
        
    }
    
    Component* refreshComponentForCell (int rowNumber, int columnId, bool isRowSelected,
                                        Component* existingComponentToUpdate) override
    {
        return nullptr;
    }
    
    int getColumnAutoSizeWidth (int columnId) override
    {
        
    }
    
    void selectedRowsChanged (int lastRowSelected) override
    {
        scriptModel->setLiveSlide (lastRowSelected);
    }
    
    void cellDoubleClicked (int rowNumber, int columnId, const MouseEvent&) override
    {
        
    }
    
    void returnKeyPressed (int lastRowSelected) override
    {
        
    }
    
private:
    SharedResourcePointer<ScriptModel> scriptModel;
    
};

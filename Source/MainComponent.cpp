/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent()
{
    addAndMakeVisible (scriptContentTable);
    setSize (600, 400);
}

MainComponent::~MainComponent()
{
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    /*
    AttributedString astr;
    astr.append("Hello, this is my ");
    astr.append("AttributedString", {32.f}, Colours::red);
    astr.append(" and I think it's ");
    astr.append("really", Font ("Arial", 12.f, Font::italic));
    astr.append(" cool!");
    
    astr.draw(g, getLocalBounds().toFloat());
     */
    
}

void MainComponent::resized()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
    scriptContentTable.setBounds (getLocalBounds());
}

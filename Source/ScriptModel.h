/*
  ==============================================================================

    ScriptModel.h
    Created: 14 Jun 2019 7:23:23pm
    Author:  Mathieu Demange

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Identifiers.h"

class DisplayArea
{
public:
    DisplayArea() {}
    DisplayArea (String n, Rectangle<float> g, float s = 1.f) : name (n), geometry (g), scale (s) {}
    ~DisplayArea() {}
    
    String name;
    Rectangle<float> geometry;
    float scale { 1.f };
    
};

class ScriptModel : public ChangeBroadcaster
{
public:
    ScriptModel() {}
    ~ScriptModel() {}
    
    void load (File file)
    {
        script = XmlDocument::parse (file);
        if (! validateScript())
        {
            DBG ("Error while validating script: " + latestValidationErrorMessage);
            script = nullptr;
            return;
        }
        source = file;
        configuration = ValueTree::fromXml (*script->getChildByName ("configuration"));
        displayAreas.clear();
        
        for (auto area : configuration.getChildWithName (IDs::displayAreas))
        {
            if (area.hasType (IDs::area))
            {
                String name = area[IDs::name];
                Rectangle<float> geometry (area.getChildWithName (IDs::geometry)[IDs::x],
                                           area.getChildWithName (IDs::geometry)[IDs::y],
                                           area.getChildWithName (IDs::geometry)[IDs::width],
                                           area.getChildWithName (IDs::geometry)[IDs::height]);
                float scale = area.getChildWithName (IDs::transform)[IDs::scale];
                
                displayAreas.set (name, DisplayArea (name, geometry, scale));
                
            }
        }
        
        DBG ("Script contains " + String (getNumSlides()) + " slides");
        
        sendChangeMessage();
    }
    
    int getNumSlides()
    {
        return script ? script->getChildByName(IDs::content)->getNumChildElements() : 0;
    }
    
    XmlElement* getSlide (int index)
    {
        return script ? script->getChildByName (IDs::content)->getChildElement (index) : nullptr;
    }
    
    Font getFontForStyle (String style)
    {
        String typefaceName = configuration.getChildWithName (IDs::styles).getChildWithName (Identifier (style))[IDs::fontFamily].toString();
        String typefaceStyle = configuration.getChildWithName (IDs::styles).getChildWithName (Identifier (style))[IDs::style].toString();
        float fontHeight = configuration.getChildWithName (IDs::styles).getChildWithName (Identifier (style))[IDs::size];
        
        return Font (typefaceName, typefaceStyle, fontHeight);
                     
    }
    
    Colour getColourForStyle (String style)
    {
        return Colour (configuration.getChildWithName (IDs::styles).getChildWithName (Identifier (style))[IDs::color].toString().getHexValue32());
    }
    
    String getPrefixForStyle (String style)
    {
        return configuration.getChildWithName (IDs::styles).getChildWithName (Identifier (style))[IDs::prefix];
    }
    
    String getDefaultDisplayArea()
    {
        return configuration.getChildWithName (IDs::displayAreas).getProperty (Identifier ("default")).toString();
    }
    
    void setLiveSlide (int index)
    {
        if (index < getNumSlides())
        {
            liveSlide = index;
            sendChangeMessage();
            DBG("sendChangeMessage()");
        }
    }
    
    int getLiveSlide()
    {
        return liveSlide;
    }
    
    File getSource()
    {
        return source;
    }
    
    void reload()
    {
        if (existsAsFile())
            load (source);
    }
    
    bool existsAsFile()
    {
        return source.existsAsFile();
    }
    
    void nextSlide()
    {
        if ((liveSlide + 1) < getNumSlides())
            setLiveSlide (liveSlide + 1);
    }
    
    void setLastRole (String name)
    {
        lastRole = name;
    }
    
    String getLastRole()
    {
        return lastRole;
    }
    
    HashMap<String, DisplayArea> displayAreas;
    
private:
    ValueTree configuration {"configuration"};
    std::unique_ptr<XmlElement> script {nullptr};
    String latestValidationErrorMessage;
    File source;
    String lastRole;
    
    int liveSlide { 0 };
    
    bool validateScript()
    {
        if (script->hasTagName (IDs::script)) // has to be <script>
        {
            if (auto version = script->getDoubleAttribute (IDs::version))
            {
                if (version == 1.0)
                {
                    // TODO: checks
                    // - only one <configuration>
                    //   - <configuration> validation etc
                    // - only one <content>
                    //   - <content> validation etc
                    
                    
                }
                else
                {
                    latestValidationErrorMessage = "Unsupported schema version";
                    return false;
                }
            }
            else
            {
                latestValidationErrorMessage = "Missing version attribute in <script>";
                return false;
            }
        }
        else
        {
            latestValidationErrorMessage = "Top element must be <script>";
            return false;
        }
     
        return true;
        
    }
    
};



/*
  ==============================================================================

    Identifiers.h
    Created: 14 Jun 2019 7:26:04pm
    Author:  Mathieu Demange

  ==============================================================================
*/

#pragma once

namespace IDs
{
    
    // tags
    const juce::Identifier script ("script");
    const juce::Identifier configuration ("configuration");
    const juce::Identifier displayAreas ("display-areas");
    const juce::Identifier area ("area");
    const juce::Identifier geometry ("geometry");
    const juce::Identifier transform ("transform");
    const juce::Identifier content ("content");
    const juce::Identifier slide ("slide");
    const juce::Identifier styles ("styles");
    
    // attributes
    const juce::Identifier version ("version");
    const juce::Identifier displayArea ("display-area");
    const juce::Identifier halign ("halign");
    const juce::Identifier valign ("valign");
    const juce::Identifier name ("name");
    const juce::Identifier x ("x");
    const juce::Identifier y ("y");
    const juce::Identifier width ("width");
    const juce::Identifier height ("height");
    const juce::Identifier scale ("scale");
    const juce::Identifier fontFamily ("font-family");
    const juce::Identifier size ("size");
    const juce::Identifier style ("style");
    const juce::Identifier color ("color");
    const juce::Identifier role ("role");
    const juce::Identifier prefix ("prefix");
    
}

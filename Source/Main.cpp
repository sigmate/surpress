/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a JUCE application.

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "MainComponent.h"
#include "Projector.h"
#include "ScriptModel.h"
#include "ScriptEditor.h"

//==============================================================================
class SurpressApplication  : public JUCEApplication
{
public:
    //==============================================================================
    SurpressApplication() {}
    
    enum CommandIDs
    {
        fileNewScript = 1,
        fileOpenScript,
        fileSaveScriptAs,
        fileSaveScript,
        fileReloadScript,
        scriptNextSlide,
        windowProjector,
        windowEditor,
    };
    
    ApplicationCommandManager commandManager;
    ApplicationProperties applicationProperties;

    const String getApplicationName() override       { return ProjectInfo::projectName; }
    const String getApplicationVersion() override    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed() override       { return true; }

    //==============================================================================
    void initialise (const String& commandLine) override
    {
        PropertiesFile::Options storageParameters;
        storageParameters.applicationName = ProjectInfo::projectName;
        storageParameters.filenameSuffix = ".settings";
        storageParameters.folderName = ProjectInfo::companyName;
        storageParameters.osxLibrarySubFolder = "Application Support";
        applicationProperties.setStorageParameters(storageParameters);
        
        mainWindow.reset (new MainWindow (getApplicationName()));
        mainWindow->restoreWindowStateFromString (applicationProperties.getUserSettings()->getValue ("mainWindowState"));
        
        projectorWindow.reset (new ProjectorWindow (getApplicationName() + " - Projector"));
        projectorWindow->restoreWindowStateFromString (applicationProperties.getUserSettings()->getValue ("projectorWindowState"));
        
        scriptEditorWindow.reset (new ScriptEditorWindow (getApplicationName() + " - Script Editor"));
        scriptEditorWindow->restoreWindowStateFromString (applicationProperties.getUserSettings()->getValue ("scriptEditorWindowState"));
        
        commandManager.registerAllCommandsForTarget (this);
        
        scriptModel->load (File ("/Users/mathieudemange/Dev/surpress/Scripts/saint-jean.xml"));
    }

    void shutdown() override
    {
        applicationProperties.getUserSettings()->setValue ("mainWindowState", mainWindow->getWindowStateAsString());
        applicationProperties.getUserSettings()->setValue ("projectorWindowState", projectorWindow->getWindowStateAsString());
        applicationProperties.getUserSettings()->setValue ("scriptEditorWindowState", scriptEditorWindow->getWindowStateAsString());
        applicationProperties.getUserSettings()->saveIfNeeded();

        mainWindow = nullptr; // (deletes our window)
        projectorWindow = nullptr;
        scriptEditorWindow = nullptr;
        
    }

    //==============================================================================
    void systemRequestedQuit() override
    {
        // This is called when the app is being asked to quit: you can ignore this
        // request and let the app carry on running, or call quit() to allow the app to close.
        quit();
    }

    void anotherInstanceStarted (const String& commandLine) override
    {
        // When another instance of the app is launched while this one is running,
        // this method is invoked, and the commandLine parameter tells you what
        // the other instance's command-line arguments were.
    }
    
    ApplicationCommandTarget* getNextCommandTarget() override
    {
        /** We do not pass any next command target at the moment. */
        return nullptr;
    }
    
    void getAllCommands (Array<CommandID>& c) override
    {
        Array<CommandID> commands {
            CommandIDs::fileNewScript,
            CommandIDs::fileOpenScript,
            CommandIDs::fileSaveScriptAs,
            CommandIDs::fileSaveScript,
            CommandIDs::fileReloadScript,
            CommandIDs::scriptNextSlide,
            CommandIDs::windowProjector,
            CommandIDs::windowEditor
        };
        c.addArray (commands);
    }
    
    void getCommandInfo (CommandID commandID, ApplicationCommandInfo& result) override
    {
        switch (commandID)
        {
                // File menu
            case CommandIDs::fileNewScript:
                result.setInfo ("New Script", "Opens a new script", "Menu", 0);
                result.addDefaultKeypress ('n', ModifierKeys::commandModifier);
                break;
            case CommandIDs::fileOpenScript:
                result.setInfo ("Open Document", "Opens an existing script", "Menu", 0);
                result.addDefaultKeypress ('o', ModifierKeys::commandModifier);
                break;
            case CommandIDs::fileSaveScriptAs:
                result.setInfo ("Save Script As...", "Saves the current script, asking for its filename", "Menu", 0);
                result.addDefaultKeypress ('s', ModifierKeys::shiftModifier | ModifierKeys::commandModifier);
                break;
            case CommandIDs::fileSaveScript:
                result.setInfo ("Save Script", "Saves the current script", "Menu", 0);
                result.setActive (scriptModel->existsAsFile());
                result.addDefaultKeypress ('s', ModifierKeys::commandModifier);
                break;
            case CommandIDs::fileReloadScript:
                result.setInfo ("Reload Script", "Reloads the current script", "Menu", 0);
                result.setActive (scriptModel->existsAsFile());
                result.addDefaultKeypress ('r', ModifierKeys::commandModifier);
                break;
            case CommandIDs::scriptNextSlide:
                result.setInfo ("Next Slide", "Advance to the next slide in script", "Menu", 0);
                //result.setActive (scriptModel->existsAsFile());
                result.addDefaultKeypress (KeyPress::spaceKey, ModifierKeys::noModifiers);
                break;
            case CommandIDs::windowProjector:
                result.setInfo ("Projector", "Show/hide projector window", "Menu", 0);
                result.setTicked (projectorWindow->isVisible());
                result.addDefaultKeypress ('p', ModifierKeys::commandModifier);
                break;
            case CommandIDs::windowEditor:
                result.setInfo ("Editor", "Show/hide built-in editor window", "Menu", 0);
                result.setTicked (scriptEditorWindow->isVisible());
                result.addDefaultKeypress ('e', ModifierKeys::commandModifier);
                break;
            default:
                break;
        }
    }
    
    bool perform (const InvocationInfo& info) override
    {
        switch (info.commandID)
        {
            case CommandIDs::fileNewScript:
                //newDocument();
                break;
            case CommandIDs::fileOpenScript:
                //openDocument();
                break;
            case CommandIDs::fileSaveScriptAs:
                //saveDocumentAs();
                break;
            case CommandIDs::fileSaveScript:
                //saveDocument();
                break;
            case CommandIDs::fileReloadScript:
                scriptModel->reload();
                break;
            case CommandIDs::scriptNextSlide:
                scriptModel->nextSlide();
                break;
            case CommandIDs::windowProjector:
                projectorWindow->setVisible (! projectorWindow->isVisible());
                break;
            case CommandIDs::windowEditor:
                scriptEditorWindow->setVisible (! scriptEditorWindow->isVisible());
                break;
            default:
                AlertWindow::showMessageBox(AlertWindow::InfoIcon, "Not implemented", "The requested action is not implemented yet.");
                // We should return false here, which would cause a JUCE assertion telling us we did not performed the request action well (which is wise). We deliberately return true and let the user know about an implemented feature to avoid confusion between disabled menu items (which could happen when implemented) and not implemented features.
                return true;
        }
        
        return true;
    }

    //==============================================================================
    /*
        This class implements the desktop window that contains an instance of
        our MainComponent class.
    */
    class MainWindow    : public DocumentWindow
    {
    public:
        MainWindow (String name)  : DocumentWindow (name,
                                                    Desktop::getInstance().getDefaultLookAndFeel()
                                                                          .findColour (ResizableWindow::backgroundColourId),
                                                    DocumentWindow::allButtons)
        {
            setUsingNativeTitleBar (true);
            setContentOwned (new MainComponent(), true);

           #if JUCE_IOS || JUCE_ANDROID
            setFullScreen (true);
           #else
            setResizable (true, true);
            setBounds (10, 400, 600, 400);
           #endif
            addKeyListener (SurpressApplication::getApplicationInstance()->commandManager.getKeyMappings());

            setVisible (true);
        }

        void closeButtonPressed() override
        {
            JUCEApplication::getInstance()->systemRequestedQuit();
        }

    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };
    
    class ProjectorWindow : public DocumentWindow
    {
    public:
        ProjectorWindow (String name) : DocumentWindow (name,
                                                        Desktop::getInstance().getDefaultLookAndFeel()
                                                                              .findColour (ResizableWindow::backgroundColourId),
                                                        DocumentWindow::allButtons)
        {
            setUsingNativeTitleBar (true);
            setContentOwned (new Projector(), true);
            setResizable (true, true);
            setBounds (700, 400, 640, 480);
            setVisible (true);
            addKeyListener (SurpressApplication::getApplicationInstance()->commandManager.getKeyMappings());
        }
        
        void closeButtonPressed() override
        {
            setVisible (false);
        }
        
    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProjectorWindow)
    };
    
    class ScriptEditorWindow : public DocumentWindow
    {
    public:
        ScriptEditorWindow (String name) : DocumentWindow (name,
                                                        Desktop::getInstance().getDefaultLookAndFeel()
                                                        .findColour (ResizableWindow::backgroundColourId),
                                                        DocumentWindow::allButtons)
        {
            setUsingNativeTitleBar (true);
            setContentOwned (new ScriptEditor(), true);
            setResizable (true, true);
            setBounds (50, 50, 640, 480);
            setVisible (true);
            addKeyListener (SurpressApplication::getApplicationInstance()->commandManager.getKeyMappings());
        }
        
        void closeButtonPressed() override
        {
            setVisible (false);
        }
        
    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ScriptEditorWindow)
    };
    
    static SurpressApplication* getApplicationInstance()
    {
        return dynamic_cast<SurpressApplication*>(getInstance());
    }

private:
    std::unique_ptr<MainWindow> mainWindow;
    std::unique_ptr<ProjectorWindow> projectorWindow;
    std::unique_ptr<ScriptEditorWindow> scriptEditorWindow;
    SharedResourcePointer<ScriptModel> scriptModel;

};

//==============================================================================
// This macro generates the main() routine that launches the app.
START_JUCE_APPLICATION (SurpressApplication)

/*
  ==============================================================================

    Projector.h
    Created: 14 Jun 2019 7:12:15pm
    Author:  Mathieu Demange

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ScriptModel.h"

//==============================================================================
/*
*/
class Projector    : public Component,
                     public ChangeListener
{
public:
    Projector()
    {
        // In your constructor, you should add any child components, and
        // initialise any special settings that your component needs.
        setSize (640, 480);
        scriptModel->addChangeListener (this);

    }

    ~Projector()
    {
    }
    
    void changeListenerCallback (ChangeBroadcaster* source) override
    {
        if (source == scriptModel)
            repaint();
    }

    void paint (Graphics& g) override
    {

        g.fillAll (Colours::black);   // clear the background

        if (showDisplayAreas)
        {
            g.setColour (Colours::red);
            for (auto displayArea : scriptModel->displayAreas)
            {
                g.drawRect (displayArea.geometry, 4.f);
                g.setFont (14.0f);
                g.drawText (displayArea.name, displayArea.geometry,
                            Justification::centred, true);   // draw some placeholder text
            }
        }
        
        auto currentSlide = scriptModel->getSlide (scriptModel->getLiveSlide());
        if (currentSlide == nullptr)
            return;
        
        AttributedString text;
        String displayAreaAttr = currentSlide->getStringAttribute(IDs::displayArea, scriptModel->getDefaultDisplayArea());
        DisplayArea displayArea = scriptModel->displayAreas[displayAreaAttr];
        
        for (int i = 0; i < currentSlide->getNumChildElements(); ++i)
        {
            auto part = currentSlide->getChildElement (i);
            String role = part->getStringAttribute (IDs::role, "");
            String style = part->getTagName();
            String prefix = role == scriptModel->getLastRole() ? "" : scriptModel->getPrefixForStyle (style);
            text.append(prefix + part->getAllSubText(), scriptModel->getFontForStyle (style), scriptModel->getColourForStyle (style));
            text.append ("\n");
            scriptModel->setLastRole (role);
        }
        
        text.setJustification (Justification::centred); // TODO: from XML
        text.draw(g, displayArea.geometry.toFloat());
    }

    void resized() override
    {
        // This method is where you should set the bounds of any child
        // components that your component contains..

    }

private:
    
    bool showDisplayAreas { false };
    
    SharedResourcePointer<ScriptModel> scriptModel;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Projector)
};
